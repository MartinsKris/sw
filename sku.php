<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
  </head>
  <body>
    <?php
      include 'db_connect.php';
      $conn=OpenCon();

      if($conn === false) {
        die("ERROR: Could not connect. " . $conn->connect_error);
      }
      $sku = $conn -> real_escape_string($_REQUEST['sku']);

      $sql="INSERT INTO productlist (sku) VALUES ('$sku');";

      if($conn -> multi_query($sql) == true) {
        header("Location: add.php");
      } else {
        echo "ERROR: Could not able to execute $sql. " . $conn -> error;
      }
      $conn->close();
    ?>
  </body>
</html>