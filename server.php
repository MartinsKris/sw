<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
  </head>
  <body>
    <?php
      include 'db_connect.php';
      $conn=OpenCon();
      if($conn === false) {
        die("ERROR: Could not connect. " . $conn->connect_error);
      }
      $sku = $conn -> real_escape_string($_REQUEST['sku']);
      $itemname = $conn -> real_escape_string($_REQUEST['itemname']);
      $price = $conn -> real_escape_string($_REQUEST['price']);
      $category = $conn -> real_escape_string($_POST['subject']);
      $dimsDVD = $conn -> real_escape_string($_REQUEST['dimsDVD']);
      $heightFRN = $conn -> real_escape_string($_REQUEST['heightFRN']);
      $widthFRN = $conn -> real_escape_string($_REQUEST['widthFRN']);
      $lengthFRN = $conn -> real_escape_string($_REQUEST['lengthFRN']);
      $weightB = $conn -> real_escape_string($_REQUEST['weightB']);    
      $glue = 'x';
      $cm = 'cm';
      $dvd = 'DVD';
      $bk = 'BOK';
      $mb = 'mb';
      $frn = 'FRN';
      $kg = 'kg';
      $valD = 'Size: ';
      $valB = 'Weight: ';
      $valF = 'Dimensions: ';
      
      if ($_POST['subject'] == 'dvd') {
        $mainsku = $dvd.$sku;
        $midledims = $_POST['dimsDVD'].$mb;
        $maindims = $valD.$midledims;
      }
      elseif ($_POST['subject'] == 'book') {
        $mainsku = $bk.$sku;
        $midledims = $_POST['weightB'].$kg;
        $maindims = $valB.$midledims;
      }
      elseif ($_POST['subject'] == 'furniture') {
        $mainsku = $frn.$sku;
        $midledims = $_POST['heightFRN'].$glue.$_POST['lengthFRN'].$glue.$_POST['widthFRN'].$cm;
        $maindims = $valF.$midledims;
      };
      
      $sql="UPDATE productlist SET mainsku = '$mainsku', itemname = '$itemname', price = '$price', category = '$category', dimsDVD = '$dimsDVD', heightFRN = '$heightFRN', widthFRN = '$widthFRN', lengthFRN = '$lengthFRN', weightB = '$weightB', maindims = '$maindims' WHERE sku = '$sku';";

      if ($conn -> multi_query($sql) == true) {
        header("Location: list.php");
      } else {
        echo "ERROR: Could not able to execute $sql. " . $conn -> error;
      }
        
      $conn->close();
    ?>
  </body>
</html>