<?php
  include 'db_connect.php';
  $conn=OpenCon();
  if($conn === false) {
    die("ERROR: Could not connect. " . $conn->connect_error);
  }

  $sql = "DELETE FROM productlist WHERE mainsku IS NULL"; 

  if (mysqli_query($conn, $sql)) {
    mysqli_close($conn);
    header('Location: list.php');
    exit;
  } else {
    echo "Error deleting record";
  }
  mysqli_close($conn);
?>