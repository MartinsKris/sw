<!DOCTYPE html>
<html lang="en">
  <head>
  </head>
  <body>
    <?php
        include 'db_connect.php';
        $conn=OpenCon();
        if($conn === false){
          die("ERROR: Could not connect. " . $conn->connect_error);
        }

        $sql="DELETE FROM productlist WHERE mainsku IS NULL";

        if($conn -> multi_query($sql)==true){
            header("Location: list.php");
        } else{
            echo "ERROR: Could not able to execute $sql. " . $conn -> error;
        }
         
        $conn->close();
    ?>
  </body>
</html>