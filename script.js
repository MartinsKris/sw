function onLoadCheck() {
  if(performance.navigation.type ==2) {
    var myWindow = window.open("empty_delete.php", "_self");
  }
}

$(document).ready(function () {
  $('.group').hide();
  $('#sample').show();
  $('#subject').change(function () {
    $('.group').hide();
    $('#'+$(this).val()).show();
  });
});

$(document).ready(function() {
  $('.checkboxdlt').click(function() {
    if($(this).is(":checked")) {
      $(this).addClass("deleteRow");
    } 
    else {
      $(this).removeClass("deleteRow");
    }
  });
});

$(function() {
  $('#priceInput').bind('input propertychange', function() {
    var priceInput = document.forms["addForm"]["price"].value;
    if (priceInput !== "" || priceInput !== null) {
      $("#priceAlert").remove();
      $("input[type='submit']").removeAttr("disabled");
    }
  });
});

$(function() {
  $('#itemname').bind('input propertychange', function() {
    var itemName = document.forms["addForm"]["itemname"].value;
    if (itemName !== "" || itemName !== null) {
      $("#nameAlert").remove();
    }
  });
});

$(document).ready(function() {
  document.getElementById("addForm").onsubmit = function () {
    var priceInput = document.forms["addForm"]["price"].value;
    var itemName = document.forms["addForm"]["itemname"].value;
    var mbInput = document.forms["addForm"]["dimsDVD"].value;
    var hInput = document.forms["addForm"]["heightFRN"].value;
    var wInput = document.forms["addForm"]["widthFRN"].value;
    var lInput = document.forms["addForm"]["lengthFRN"].value;
    var weInput = document.forms["addForm"]["weightB"].value;
    var submit = true;
    var typeswitchoption = $( "#subject" ).val();

    if (priceInput === null || priceInput === "") {
      priceError = "Please, submit required data";
      document.getElementById("price_error").innerHTML = priceError;
      submit = false;
    }
    if (itemName === null || itemName === "") {
      nameError = "Please, submit required data";
      document.getElementById("itemname_error").innerHTML = nameError;
      submit = false;
    }
    if (typeswitchoption == "sample") {
      typeError = "Please, submit required data";
      document.getElementById("subject_error").innerHTML = typeError;
      submit = false;
    }
    if(typeswitchoption == 'dvd') {
      if (mbInput === null || mbInput === "") {
        dvdError = "Please, submit required data";
        document.getElementById("dimensions_error").innerHTML = dvdError;
        submit = false;
      }
    }
    if(typeswitchoption == 'furniture') {
      if ((hInput === null || hInput === "")&&(wInput === null || wInput === "")&&(lInput === null || lInput === "")) {
        furnitureError = "Please, submit required data";
        document.getElementById("dimensions_error").innerHTML = furnitureError;
        submit = false;
      }
    }
    if(typeswitchoption == 'book') {
      if (weInput === null || weInput === "") {
        bookError = "Please, submit required data";
        document.getElementById("dimensions_error").innerHTML = bookError;
        submit = false;
      }
    }

    return submit;
  }
});

function removeWarning() {
  document.getElementById(this.id + "_error").innerHTML = "";
}

function removeWarningS() {
  document.getElementById("subject_error").innerHTML = "";
}

function removeDimWarning() {
  document.getElementById("dimensions_error").innerHTML = "";  
}

$('input propertychange', function() {
  document.getElementById("price").oninput = removeWarning;
  document.getElementById("itemname").oninput = removeWarning;
  document.getElementById("subject").onchange = removeWarningS;
  document.getElementById("dimsDVD").oninput = removeDimWarning;
  document.getElementById("heightFRN" && "widthFRN"&&"lengthFRN").oninput = removeDimWarning;
  document.getElementById("weightB").oninput = removeDimWarning;
});

$(function() {
  var $price = $('#price');
  var numberTest = /^\d+\.{0,1}\,{0,1}\d{0,2}$/;
  $('#price').bind('input propertychange', function() {
    if (numberTest.test($(this).val())) {
      document.getElementById("price").oninput = removeWarning;
    }
    else {
      $price.val($price.val().slice(0, -1));
      priceError = "Please, provide the data of indicated type";
      document.getElementById("price_error").innerHTML = priceError;
    }
  });
});

$(function() {
  var numberTest = /^\d+\.{0,1}\,{0,1}\d{0,2}$/;
  $('#heightFRN, #widthFRN, #lengthFRN, #weightB').bind('input propertychange', function() {
    if (numberTest.test($(this).val())) {
      $(this).oninput = removeDimWarning;
    }
    else {
      $(this).val($(this).val().slice(0, -1));
      maindimsError = "Please, provide the data of indicated type";
      document.getElementById("dimensions_error").innerHTML = maindimsError;
    }
  });
});

$(function() {
  var numberTest = /^-?\d*$/;
  $('#dimsDVD').bind('input propertychange', function() {
    if (numberTest.test($(this).val())) {
      $(this).oninput = removeDimWarning;
    }
    else {
      $(this).val($(this).val().slice(0, -1));
      maindimsError = "Please, provide the data of indicated type";
      document.getElementById("dimensions_error").innerHTML = maindimsError;
    }
  });
});