<!DOCTYPE html>
<html>
  <head>
    <title>Product Add</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="script.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
    <script>
      window.addEventListener('pagehide', function() {
        window.open("empty_delete.php", "_top");
      });
    </script>
  </head>
  <body>
    <div class="navList">
      <div class="logo">
        <h2>Product Add</h2>
      </div>
      <div class="buttons">
        <form action="sku_delete.php" method="post">
      <input type="submit" name="skuDelete" id="skuDelete" value="Cancel" class="indexButtons">
      </form>
      </div>
    </div>
    <div class="mainAdd">
      <form action="server.php" method="post" type="post" rel="noopener" name="addForm"  id="addForm" onsubmit="validateDimsPrice()">
      <div class="inputField">
        <label for="sku">SKU</label>
          <?php
            include 'db_connect.php';
            $conn = OpenCon();

            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }
            $sql = "SELECT sku FROM productlist WHERE mainsku IS NULL";
            $result = $conn->query($sql);
            $dbSKU = '';
            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { 
                $dbSKU .= ' 
                  <input class="input" readonly name="sku" type="text"value="'.$row['sku'].'" id="'.$row['sku'].'"/>
                '; 
              }
              echo $dbSKU;
            }
            else {
              echo "0 results";
            };
            $conn->close();
          ?>
          </div>
        <div class="inputField">
          <label for="itemname">Name</label>
          <input class="input" type="text" name="itemname" id="itemname">
          <span class="error"><p id="itemname_error"></p></span>
        </div>
        <div class="inputField">
          <label for="price">Price ($)</label>
          <input class="input" type="text" name="price" id="price" onkeyup="this.value = this.value.replace(/,/g, '.')" onchange="this.value = this.value.replace(/,/g, '.')">
          <span class="error"><p id="price_error"></p></span>
        </div>
        <div class="inputField">
          <label for="typeInput">Type Switcher</label>
          <p id="typeInput">
            <select name="subject" id="subject">
            <option for="dvd" name="dvd" value="dvd" selected="selected">DVD</option>
            <option for="book" name="book" value="book" selected="selected">Book</option>
            <option for="furniture" name="furniture" value="furniture" selected="selected">Furniture</option>
            <option value="sample" selected="selected" style="display:none;">Type switcher</option>
            </select>
          </p>
          <span class="error"><p id="subject_error"></p></span>
        </div>
        <!--dvd-->
        <div class="inputField">
          <p class="group" id="dvd">
            <label for="dimsDVD">Size (MB)</label>
            <input class="input" type="text" name="dimsDVD" id="dimsDVD" placeholder="Please provide size">
          </p>
        </div>
        <!--book-->
        <div class="inputField">
          <p class="group" id="book">
            <label for="weightB">Weight (KG)</label>
            <input class="input" type="text" name="weightB" id="weightB" placeholder="Please provide weight" onkeyup="this.value = this.value.replace(/,/g, '.')"
            onchange="this.value = this.value.replace(/,/g, '.')"/>
          </p>
        </div>
        <!--furniture-->
        <div class="inputField">
          <p class="group" id="furniture">
            <label for="heightFRN">Height (CM)</label>
            <input class="input" type="text" name="heightFRN" id="heightFRN" placeholder="Please provide dimensions" onkeyup="this.value = this.value.replace(/,/g, '.')" onchange="this.value = this.value.replace(/,/g, '.')"/><br><br>
            <label for="widthFRN">Width (CM)</label>
            <input class="input" type="text" name="widthFRN" id="widthFRN" placeholder="Please provide dimensions" onkeyup="this.value = this.value.replace(/,/g, '.')" onchange="this.value = this.value.replace(/,/g, '.')"/><br><br>
            <label for="lengthFRN">Length (CM)</label>
            <input class="input" type="text" name="lengthFRN" id="lengthFRN" placeholder="Please provide dimensions" onkeyup="this.value = this.value.replace(/,/g, '.')" onchange="this.value = this.value.replace(/,/g, '.')"/>
          </p>
        </div>
        <span class="error"><p id="dimensions_error"></p></span>

        <input type="submit" value="Save" id="submitButton" class="indexButtons">
      </form>
      <div class="footer">
        <p>Scandiweb Test assignment</p>
      </div>
    </div>
  </body>
</html>