<!DOCTYPE html>
<html lang="en">
<html>
  <head>
    <title>Product List</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="script.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
  </head>
  <body onpageshow="onLoadCheck()">
    <div class="navList">
      <div class="logo">
        <h2>Product List</h2>
      </div>
      <div class="buttons">
        <form action="sku.php" method="post">
        <input type="submit" name="add" id="add" value="ADD" class="indexButtons">
        </form>
      </div>
    </div>
    <div class="main">
      <form action="delete.php" method="post" rel="noopener">
        <input type="submit" name="delete" id="delete" value="MASS DELETE" class="indexButtons">

        <?php
          include 'db_connect.php';
          $conn = OpenCon();
          if ($conn->connect_error) 
          {
            die("Connection failed: " . $conn->connect_error);
          }
          $sql = "SELECT mainsku, itemname, price, maindims FROM productlist";
          $result = $conn->query($sql);
          $dbData = '';
          if ($result->num_rows > 0) 
          {
            while($row = $result->fetch_assoc()) 
            { 
              $dbData .= ' 
              <div class="window">
                <input name="checkbox[]" type="checkbox" class="checkboxdlt" value="'.$row['mainsku'].'" id="'.$row['mainsku'].'"/>
                <div class="values">
                    <div class="inputValue">'.$row['mainsku'].'</div> 
                    <div class="inputValue">'.$row['itemname'].'</div> 
                    <div class="inputValue">'.$row['price']." $".'</div> 
                    <div class="inputValue">'.$row['maindims'].'</div>
                </div>
              </div> 
              '; 
            }
            echo $dbData;
          }
          else 
          {
            echo "0 results";
          };
          $conn->close();
        ?>
      </form>
      <div class="footer">
        <p>Scandiweb Test assignment</p>
      </div>
    </div>
  </body>
</html>